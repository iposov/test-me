---
type: tasks
---

## Задачи с сайта Coding.bat
   
   [Codingbat](http://codingbat.com)
   
    Зарегистрируйтесь на этом сайте и решайте задачи из разделов
    `Warmup-*`, `String-*`, `Logic-*`, `String-*`, `Array-*`.
    Из каждого раздела необходимо решить хотя бы $6 - x$ задач, где
    $x$ это сложность раздела, например, у раздела `String-3` она равна 3.
 
## Задачки на базовые возможности Java, теория чисел
    
1. `PrimalityTest`. Дано число, верните, простое ли оно
1. `SieveOfEratosthenes`. Алгоритм [Решето Эратосфена](https://neerc.ifmo.ru/wiki/index.php?title=%D0%A0%D0%B5%D1%88%D0%B5%D1%82%D0%BE_%D0%AD%D1%80%D0%B0%D1%82%D0%BE%D1%81%D1%84%D0%B5%D0%BD%D0%B0), еще одно [описание с картинками](https://ru.wikipedia.org/wiki/%D0%A0%D0%B5%D1%88%D0%B5%D1%82%D0%BE_%D0%AD%D1%80%D0%B0%D1%82%D0%BE%D1%81%D1%84%D0%B5%D0%BD%D0%B0).
    1. Дано натуральное `n`, верните массив `boolean[]` размера `n + 1`, в котором по индексу
    `i` записано, верно ли что число `i` простое.
    1. Дано натуральное `n`, верните массив `int[]`, в котором перечислены в порядке возрастания
    все простые числа от 1 до `n`. Воспользуйтесь функцией из предыдущего подпункта.
    
## Массивы.

Создайте класс `ArraysTasks` и заполните его функциями:
    
1. Дано `n`, верните массив из первых n четных чисел: 2 4 6 8 ... 2n
1. Дан массив, функция проверяет, есть ли в массиве одинаковые элементы: `boolean hasSimilar(int[] a)`
1. Дан массив, верните среднее значение всех элементов массива. (т.е. сумму элементов, деленную на количество элементов)
1. Остортируйте массив по убыванию: `private static int[] sortReverse(int[] a)`. Используйте для
этого `Arrays.sort()` с одним аргументом. Содержимое исходного массива меняться
не должно, нужно вернуть новый отсортированный массив. Для копирования массива используйте и `Arrays.copyOf()`.

Пример:
    ```
    int[] x = {20, 10, 30};
    int[] y = sortReverse(x);
    System.out.println(Arrays.toString(x)); // печатает 20 10 30
    System.out.println(Arrays.toString(y)); // печатает 30 20 10
        ```
## Чтение и запись в файлы

Создайте класс `IOTasks` и заполните его функциями:

1. Дан файл, содержащий несколько целых чисел, разделенных пробелами и переводами строк.
Прочитайте из него все числа и выведите на экран их сумму.
    1. Решите предыдущую задачу, но считайте, что в файле могут быть и слова, не являющиеся числами.
    Эти слова надо игнорировать. Используйте `scanner.hasNextInt()`, не используйте
    `scanner.useDelimiter()`.
1. Дан массив строк `lines` и имя файла. Запишите в файл заданные строки построчно.
Не забудьте, что есть цикл "for each".
1. Даны два файла. Прочитайте из первого файла текст и перепишите его во второй файл,
исправив ошибки в регистрах букв. Например, если исходный текст был:

    ```
    какой-то Текст с неправильными   Регистрами букв!  втОРОе предложение Этого текста.
    ```

    то должно получиться:

    ```
    Какой-то текст с неправильными регистрами букв! Второе предложение этого текста.
    ```

    Вы должны читать исходный файл по словам, разделенными пробельными символами, и
    следить за последним символом очередного слова. Если это символ конца предложения
    (точка, восклицательный знак, вопросительный знак), нужно сделать так, чтобы следующее слово
    начиналось с заглавной буквы. Иначе следующее слово должно начинаться со строчной буквы.
    Заглавные буквы внутри слов нужно изменить на строчные.

1. *Скорочтение*. Класс `FastReader`. Прочитать из файла текст, разбить его на слова.
Показыавть в консоли слова текста с большой скоростью. Т.е. программа показывает первое слово, делает паузу, показывает вместо него второе слово, потом пауза, третье слово и т.д.
Если после слова стоит знак препинания, его нужно отображать. После знаков препинания (запятых, точек и т.п.) нужно чуть увеличивать паузу.
    * Для того, чтобы перезаписать слово в консоли, выводите символ `'\r'`, этот символ не переводит строку, а возвращает каретку в начало.
    * Вызывайте метод `System.out.flush()` после каждого вызова `System.out.print()`. Иначе результат может не появиться на экране.
    
## Двумерные массивы

Создайте класс `TwoDimensionalArrayTask`, запишите следующий код в метод
`main` и заставьте его работать:

    ```
    char[][] c = createTable(20, '.');
    printTable(c);

    System.out.println("============ Заполним строки: ==========");
    fillFirstAndLastLines(c, '#');
    printTable(c);

    System.out.println("============ Заполним столбцы: =========");
    fillFirstAndLastColumns(c, '#');
    printTable(c);
    ```
1. `createTable(20, '.')` возвращает массив char 20 на 20, все символы в массиве должны быть '.'
1. `printTable` печатает массив на экран. При этом выводите символы подряд, без запятых.
Можете вывести пробелы между символами, так получается более красиво.
1. `fillFirstAndLastLines` Воспользуйтесь `Arrays.fill()`, чтобы заполнить первую и последнюю строки
массива символами '#'.
1. `fillFirstAndLastColumns` Заполните первый и последний столбец массива символами '#'.

## Регулярные выражения.
1. Дана строка, проверьте, что в ней содержится корректный email адрес. Будем считать, что корректный
   email состоит из имени пользователя (несколько латинских букв, точек, подчеркиваний, минусов),
   далее следует символ @, далее идет домен (тоже несколько латинских букв, точек, подчеркиваний,
   минусов), в конце должна быть точка и от двух до четырех латинских букв.
   Т.е. конец должен выглядеть как .com, .ru и т.п. Помните, что обычная точка означает любой символ,
   и ее может понадобиться экранировать. Используйте метод `matches()` класса String
   1. Решите прошлую задачу еще раз, но используйте регулярное выражение для email по ссылке
   [emailregex.com](https://emailregex.com/)
1. Дана строка с текстом на русском языке, в которой автор неправильно расставил пробелы перед запятыми. Например, `Это строка  , у которой зачем-то написаны два пробела перед запятой`. Нужно найти все пробельные символы перед запятыми и удалить их. Должно получиться `Это строка, у которой зачем-то написаны два пробела перед запятой`. Используйте метод `replaceAll()` класса String.
1. Дана строка. Найдите в ней все слова, написанные через дефис и поменяйте две половинки этих слов местами. Например, строка "Какая-то сине-зеленовая трава" должна превратиться в "то-Какая зеленовая-сине трава". Используйте метод `replaceAll` класса String и `$` для ссылки на группы.
1. Дана строка. Посчитайте, сколько раз в ней встречаются символы "кот", в произвольном регистре. Т.е. Кот и КОТ тоже надо считать. Используйте `Matcher` с методом `find()`. При создании регулярного выражения с
помощью класса `Pattern` установите режимы `Pattern.UNICODE_CASE` и `Pattern.CASE_INSENSITIVE`.
    
        * Необязательное задание. А если надо искать слово "кот", т.е. не считать, например, слово "который"? Попробуйте два способа указать, что "кот" должно быть частью другого слова. 1) найдите на странице с лекцией ссылку на все возможности регулярных выражений в Java, и там найдите,
        как указать word boundaries 2) используйте конструкции
        наподобие (\s\|$) - это означает либо пробельный символ, либо конец строки.
1. Даны два имени файла. Откройте первый, замените в нем все числа (последовательности цифр) на 
эти же числа, увеличенные на один. Запишите результат во второй файл. Используйте функцию
`Files.readString(Path, DefaultEncoding.UTF-8)` для чтения файла, прописав необходимые `import`.
Для записи файла найдите аналогичную функцию.